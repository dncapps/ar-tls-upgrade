AR-tls-upgrade
=========

Tasks required to update Netsuite Terminals to the tls 1.2 standard. Example of a role that can be removed when the project is finished.

Requirements
------------

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

License
-------

BSD

Author Information
------------------
